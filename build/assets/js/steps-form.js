$(document).ready(function() {

	var $steps = $(".step"),
			$stepBtns = $(".step-btn"),
			$nextBtn = $(".next-btn"),
			$backBtn = $(".back-btn"),
			$endBtn = $(".end-btn"),
			$retryBtn = $(".retry"),
			$socials = $(".social-input"),
			$images = $(".animal-img"),
			currentStep = 0,
			nameRgx = /^[- а-яА-ЯёЁa-zA-Z]{3,}$/,
			emailRgx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
			socialRgx = /^[-:#._/a-z0-9]{7,}$/;

	$($steps[0]).show();
	$backBtn.hide();
	$endBtn.hide();
	
	//Generate list of countries from json-file using Ajax-request
	$.getJSON("assets/ajax/countries.json", function(countries) {
		for (var key in countries) {
			$("<option/>", {
				"data-num": key,
				html: countries[key]
			}).appendTo(".form-country");
		}
	});

	//Country-select change action
	$(".form-country").change(function() {
		var countryValue = $(this).val(),
				countryNum;
		$(".form-country > option").each(function(i, elem) {
			if(countryValue == $(elem).val()) {
				countryNum = $(elem).attr("data-num");
				return false;
			}
		});
		
		$(".form-city > option:not(:first-child)").remove();

		//Generate list of appropriate cities from json-file using Ajax-request
		$.getJSON("assets/ajax/cities.json", function(cities) {
			for (var key in cities) {
				if(countryNum == cities[key].country) {
					$("<option/>", {
						html: cities[key].name
					}).appendTo(".form-city");
				}
			}
		});
	});

	//Next-button click action
	$nextBtn.click(function() {
		var $name = $(".form-name"),
				$email = $(".form-email"),
				$country = $(".form-country"),
				$city = $(".form-city"),
				$socials = $(".social-input"),
				errorsArray = [];

		if(currentStep == 0) {
			//Name validation
			if(!nameRgx.test($name.val())) {
				showError($(".name-error"), $name);
				if(emailRgx.test($email.val())) {
					hideError($(".email-error"), $email);
				}
				return false;	
			} else {
				hideError($(".name-error"), $name);
			}

			//Email validation
			if(!emailRgx.test($email.val())) {
				showError($(".email-error"), $email);
				return false;
			} else {
				hideError($(".email-error"), $email);
			}
		}
		
		if(currentStep == 1) {
			//Country validation
			if($country.val() === null) {
				showError($(".country-error"), $country);
				return false;
			} else {
				hideError($(".country-error"), $country);
			}

			//City validation
			if($city.val() === null) {
				showError($(".city-error"), $city);
				return false;
			} else {
				hideError($(".city-error"), $city);
			}
		}

		if(currentStep == 2) {
			//Social inputs validation
			$socials.each(function(i, elem) {
					if($(elem).css("display") != "none") {
						if(!socialRgx.test($(elem).val())) {
							errorsArray.push($(elem));
							showError($(".social-error:eq("+ i + ")"), $(elem));
						} else {
							hideError($(".social-error:eq("+ i + ")"), $(elem));
						}
					} 
			});
			if(errorsArray.length != 0) {
				return false;
			}
			hideError($(".social-error"), $socials);
		}
		
		if(currentStep == $steps.length - 2) {
			$(this).hide();
			$endBtn.show();
		}

		$($stepBtns[currentStep]).addClass("step-btn-validated").removeClass("step-btn-current");
		$($stepBtns[currentStep + 1]).addClass("step-btn-active").addClass("step-btn-current");

		$backBtn.show();
		currentStep++;
		changeStep(currentStep);
	});

	//Back-button click action
	$backBtn.click(function() {
		if(currentStep == 1) {
			$(this).hide();
		}

		$($stepBtns[currentStep]).removeClass("step-btn-current");
		$($stepBtns[currentStep - 1]).addClass("step-btn-current");

		$endBtn.hide();
		$nextBtn.show();
		currentStep--;
		changeStep(currentStep);
	});

	//Step-buttons click actions
	$stepBtns.click(function() {
		var newStep = +$(this).text() - 1,
				$name = $(".form-name"),
				$email = $(".form-email"),
				$country = $(".form-country"),
				$city = $(".form-city"),
				$socials = $(".social-input"),
				errorsArray = [];

		if(!($(this).hasClass("step-btn-active") || $(this).hasClass("step-btn-validated"))) {
			return false;
		}
		if($(this).hasClass("step-btn-current")) {
			return false;
		}

		if(currentStep == 0) {
			//Name validation
			if(!nameRgx.test($name.val())) {
				showError($(".name-error"), $name);
				if(emailRgx.test($email.val())) {
					hideError($(".email-error"), $email);
				}
				return false;	
			} else {
				hideError($(".name-error"), $name);
			}

			//Email validation
			if(!emailRgx.test($email.val())) {
				showError($(".email-error"), $email);
				return false;
			} else {
				hideError($(".email-error"), $email);
			}
		}
		
		if(currentStep == 1) {
			//Country validation
			if($country.val() === null) {
				showError($(".country-error"), $country);
				return false;
			} else {
				hideError($(".country-error"), $country);
			}

			//City validation
			if($city.val() === null) {
				showError($(".city-error"), $city);
				return false;
			} else {
				hideError($(".city-error"), $city);
			}
		}

		if(currentStep == 2) {
			//Social inputs validation
			$socials.each(function(i, elem) {
					if($(elem).css("display") != "none") {
						if(!socialRgx.test($(elem).val())) {
							errorsArray.push($(elem));
							showError($(".social-error:eq("+ i + ")"), $(elem));
						} else {
							hideError($(".social-error:eq("+ i + ")"), $(elem));
						}
					} 
			});
			if(errorsArray.length != 0) {
				return false;
			}
			hideError($(".social-error"), $socials);
		}

		//Change step by step-button
		(newStep == 0) ? $backBtn.hide() : $backBtn.show();
		if(!(newStep == $steps.length - 1)) {
			$endBtn.hide();
			$nextBtn.show();
		} else {
			$endBtn.show();
			$nextBtn.hide();
		}

		$($stepBtns[currentStep]).removeClass("step-btn-current");
		$($stepBtns[newStep]).addClass("step-btn-current");
		currentStep = newStep;
		changeStep(currentStep);
	});

	//Checkbox click action
	$(".checkbox-label").click(function() {
		var i = +$(this).attr("data-num"),
				$currentError = $(".social-error:eq("+ i + ")");
				$currentInput = $(".social-input:eq("+ i + ")");

		if($currentError.css("display") != "none") {
			hideError($currentError, $currentInput);
			$currentInput.val("");
		}
	});

	//Active-image click action
	$images.click(function() {
		$images.removeClass("active-img");
		$(this).addClass("active-img");
	});

	//End-button click action
	$endBtn.click(function () {
		//Image validation
		var $imgError = $(".img-error");
		var $imgActive = $(".active-img");

		if(!$images.hasClass("active-img")) {
			$imgError.text("Вы не выбрали любимого котика.").show();
			return false;
		}

		if(!($imgActive.attr("data-animal") == "cat")) {
			$imgError.text("Вы выбрали собачку. А нужно котика.").show();
			return false;
		}

		$imgError.hide();
		$($steps[currentStep]).hide();
		$(".form-wrapper").hide();

		//Generate final screen content
		$(".name-selected").text($(".form-name").val());
		$(".email-selected").text($(".form-email").val());
		$(".home-selected").text($(".form-city").val() + ", " + $(".form-country").val());
		$(".img-selected").attr("src", $(".active-img").attr("src"));

		$(".social-input").each(function(i, elem) {
			if($(elem).css("display") != "none") {
				$(".social-value:eq("+ i + ")").text($(elem).val());
				$(".social-selected:eq("+ i + ")").show();
			}
		});

		$(".final-screen-wrapper").fadeIn(300);
	});

	//Retry-button click action
	$retryBtn.click(function() {
		//Clear all filds and step-buttons
		$(".form-name").val("");
		$(".form-email").val("");
		$(".form-country").val("");
		$(".form-city").val("");
		$(".social-input").val("");
		$(".hidden-box").prop("checked", false);
		$(".active-img").removeClass("active-img");
		$(".social-selected").hide();

		$($stepBtns).removeClass("step-btn-validated").removeClass("step-btn-active").removeClass("step-btn-current");
		$($stepBtns[0]).addClass("step-btn-active").addClass("step-btn-current");

		//Retry again from first step
		$(".final-screen-wrapper").hide();
		$backBtn.hide();
		$endBtn.hide();

		$(".form-wrapper").fadeIn(300);
		$($steps[0]).fadeIn(300);
		$nextBtn.fadeIn(300);
		currentStep = 0;
	});

	function changeStep(i) {
		$steps.hide();
		$($steps[i]).fadeIn(300);
	};

	function showError($err, $obj) {
		$err.show();
		$obj.addClass("error-border");
	}

	function hideError($err, $obj) {
		$err.hide();
		$obj.removeClass("error-border");
	}

});
