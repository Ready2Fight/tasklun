var gulp         = require('gulp'),
    postcss      = require('gulp-postcss'),
    sass         = require('gulp-sass'),
    autoprefixer = require('autoprefixer'),
    browser      = require('browser-sync'), 
    sourcemaps   = require('gulp-sourcemaps'),
    panini       = require('panini');


// Copy page templates into finished HTML file
gulp.task('pages', function() {
  gulp.src('src/pages/**/*.html')
    .pipe(panini({
      root: 'src/pages/',
      layouts: 'src/layouts/',
      partials: 'src/partials/',
      helpers: 'src/helpers/',
      data: 'src/data/'
    }))
    .pipe(gulp.dest('build'));
});

// Compile Sass into CSS
gulp.task('sass', ['pages'], function () {
  return gulp.src('src/sass/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([ autoprefixer({ browsers: ['last 2 versions'] }) ]))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('build/assets/css'))
        .pipe(browser.stream({match: '**/*.css'}));
});


// Starts a BrowserSync instance
gulp.task('serve', ['sass'], function(){
  browser.init({
      server: "build"
    });
});

// Watch for changes to static assets, pages, Sass
gulp.task('default', ['serve'], function() {
  gulp.watch('src/{layouts,partials}/**/*.html').on('change', function () {
        panini.refresh();
        gulp.run('pages');
    });
  gulp.watch('build/index.html').on('change', browser.reload);
  gulp.watch(['**/sass/**/*.scss'], ['sass']);
});